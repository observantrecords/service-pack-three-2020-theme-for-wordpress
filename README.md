# SERVICE PACK THREE 2020 Theme

A custom theme for [SERVICE PACK THREE](http://servicepackthree.com/).

## Dependencies

* [Observant Records 2020 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2020-for-wordpress)
